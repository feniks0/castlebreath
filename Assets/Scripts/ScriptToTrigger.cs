using System.Linq;
using UnityEngine;

public class ScriptToTrigger : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.name.Contains("Chunk"))
        {
            return;
        }
        
        other.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        var inst = World.getInstance;
        Debug.Log(inst.listChunks.Count);
        var linqlist = inst.listChunks.Where(a => a.IsNowChunk(col));
        var chunk1 = linqlist.ToList();
        if (chunk1.Count() == 0)
        {
            return;
        }
        var newChunk = chunk1[0].EntTrigger(col);
        if (!inst.listChunks.Any(i => i.Equals(newChunk)))
        {
            inst.listChunks.Add(newChunk);
            Debug.Log($"-- {newChunk.chunkObj.transform.position} --");
        }
        else
        {
            if (newChunk is null)
            {
                return;
            }

            if (!newChunk.chunkObj.activeSelf)
            {
                newChunk.chunkObj.SetActive(true);
            }
            
        }
    }
}
