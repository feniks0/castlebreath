using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuB : MonoBehaviour
{
    public void StartButtonGame()
    {
        SceneManager.LoadScene("ProccessGame");
    }

    public void ExitButtonGame()
    {
        Application.Quit();
    }
}
