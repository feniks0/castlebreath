using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    // Ось Ox
    public int xVelocity = 5;

    // Ось Oz
    public int zVelocity = 8;
    private Rigidbody2D rigidBody;

    private void Start()
    {
        // Получаем доступ к Rigidbody2D объекта Player
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        updatePlayerPosition();
    }

    private void updatePlayerPosition()
    {
        // Получаем значение ввода горизонтального перемещение
        float moveInput = Input.GetAxis("Horizontal");
        // Получаем значение ввода прыжка

        // Значения xVelocity, yVelocity можно задать через инспектор

        if (moveInput < 0)
        {
            // Движ влево
            rigidBody.velocity = new Vector2(-xVelocity, rigidBody.velocity.y);
        }
        else if (moveInput > 0)
        {
            // Движ вправо
            rigidBody.velocity = new Vector2(xVelocity, rigidBody.velocity.y);
        }
    }
}