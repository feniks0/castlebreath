using System.Collections.Generic;

public class World
{
    private static World instance = null;
    public List<Chunk> listChunks;

    public World()
    {
        listChunks = new List<Chunk>();
    }
    public static World getInstance
    {
        get
        {
            if (instance == null)
            {
                instance = new World();
            }

            return instance;
        }
    }
}
