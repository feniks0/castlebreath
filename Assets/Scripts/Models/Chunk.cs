using System;
using System.Linq;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    public GameObject chunkObj;
    public readonly string Name;

    private static int _allCount = 0;
    private readonly Sprite _cSprite;
    private readonly GameObject _sideR;
    private readonly GameObject _sideL;
    private readonly GameObject _sideU;
    private readonly GameObject _sideD;

    public Chunk(Sprite chunkSprite, Vector3 coordinates)
    {
        this._cSprite = chunkSprite;
        chunkObj = new GameObject();
        var position = new Vector3(MathF.Round(coordinates.x, 1), MathF.Round(coordinates.y, 1), MathF.Round(coordinates.z, 1));
        chunkObj.transform.position = position;
        chunkObj.AddComponent<SpriteRenderer>().sprite = chunkSprite;
        chunkObj.AddComponent<BoxCollider2D>().isTrigger = true;
        chunkObj.name = $"Chunk {++_allCount}";
        Name = chunkObj.name;
        var boxChunk = chunkObj.GetComponent<BoxCollider2D>();

        //Add walls
        //side right
        this._sideR = new GameObject();
        this._sideR.AddComponent<BoxCollider2D>();
        InitSide(chunkObj, "sideR", this._sideR);
        var boxR = this._sideR.GetComponent<BoxCollider2D>();
        boxR.isTrigger = true;
        boxR.size = new Vector2(0.1f, boxChunk.size.y);
        this._sideR.transform.localPosition =
            new Vector3(boxChunk.bounds.extents.x - boxR.bounds.extents.x, 0, 0);

        //side left
        this._sideL = Instantiate(this._sideR);
        var boxL = _sideL.GetComponent<BoxCollider2D>();
        InitSide(chunkObj, "sideL", _sideL);
        this._sideL.transform.localPosition =
            new Vector3(boxL.bounds.extents.x - boxChunk.bounds.extents.x, 0, 0);

        //side up 
        this._sideU = Instantiate(this._sideR);
        InitSide(chunkObj, "sideU", this._sideU);
        var boxU = _sideU.GetComponent<BoxCollider2D>();
        var size = boxU.size;
        size = new Vector3(size.y, size.x);
        boxU.size = size;
        this._sideU.transform.localPosition = new Vector3(0, boxChunk.bounds.extents.y - boxU.bounds.extents.y, 0);

        //side down
        this._sideD = Instantiate(this._sideU);
        InitSide(chunkObj, "sideD", this._sideD);
        var boxD = _sideD.GetComponent<BoxCollider2D>();
        this._sideD.transform.localPosition =
            new Vector3(0, boxD.bounds.extents.y - boxChunk.bounds.extents.y, 0);
    }

    

    [CanBeNull]
    public Chunk EntTrigger(Collider2D other)
    {
        var inst = World.getInstance;
        var boxSize = this.chunkObj.GetComponent<BoxCollider2D>().bounds.size;
        var position = this.chunkObj.transform.position;
        Vector3 temp;
        if (other.gameObject.Equals(this._sideR))
        {
            inst = World.getInstance;
            temp = new Vector3(
                MathF.Round(position.x + boxSize.x, 1),
                position.y, 0);
            if (inst.listChunks.Exists(i => i.IsExistsVector(temp)))
                return inst.listChunks.FirstOrDefault(x => x.IsExistsVector(temp));
            return new Chunk(this._cSprite, temp);
        }

        if (other.gameObject.Equals(this._sideL))
        {
            temp = new Vector3(
                MathF.Round(position.x - boxSize.x, 1), position.y, 0);
            if (inst.listChunks.Exists(i => i.IsExistsVector(temp)))
            {
                return inst.listChunks.FirstOrDefault(x => x.IsExistsVector(temp));
            }
            return new Chunk(this._cSprite, temp);
        }

        if (other.gameObject.Equals(this._sideU))
        {
            temp = new Vector3(position.x,
                MathF.Round(position.y + boxSize.y, 1), 0);
            if (inst.listChunks.Exists(i => i.IsExistsVector(temp)))
            {
                return inst.listChunks.FirstOrDefault(x => x.IsExistsVector(temp));
            }
            return new Chunk(this._cSprite, temp);
        }

        if (other.gameObject.Equals(_sideD))
        {
            temp = new Vector3(position.x,
                MathF.Round(position.y - boxSize.y, 1), 0);
            if (inst.listChunks.Any(i => i.IsExistsVector(temp)))
            {
                return inst.listChunks.FirstOrDefault(x => x.IsExistsVector(temp));
            }
            return new Chunk(this._cSprite, temp);
        }

        return null;
    }

    public bool IsExistsVector(Vector3 otherVec)
    {
        return this.chunkObj.transform.position == otherVec;
    }

    public override string ToString()
    {
        return $"{this.Name}";
    }

    public override bool Equals(object other)
    {
        if (other is Chunk obj)
        {
            if (this.chunkObj.transform.position != obj.chunkObj.transform.position)
                return false;
            // if (this._sideU.transform.localPosition == obj._sideU.transform.localPosition)
            //     return false;
            // if (this._sideR.transform.localPosition == obj._sideR.transform.localPosition)
            //     return false;
            // if (this._sideL.transform.localPosition == obj._sideL.transform.localPosition)
            //     return false;
            // if (this._sideD.transform.localPosition == obj._sideD.transform.localPosition)
            //     return false;
            return true;
        }

        return false;
    }

    public override int GetHashCode()
    {
        return this.chunkObj.GetHashCode();
    }

    public bool IsNowChunk(Collider2D oneByOne)
    {
        if ((oneByOne.gameObject.Equals(this._sideR) ||
             oneByOne.gameObject.Equals(this._sideL) ||
             oneByOne.gameObject.Equals(this._sideU) ||
             oneByOne.gameObject.Equals(this._sideD)) &&
            oneByOne.gameObject.transform.parent.Equals(this.chunkObj.transform))
        {
            return true;
        }

        return false;
    }
    
    private void InitSide(GameObject parentO, string nameSide, GameObject o)
    {
        o.transform.parent = parentO.transform;
        o.name = nameSide;
    }
}